###
# Create a docker machine from zero
# Author: luizstacio
##

DOCKER_USER=""
DOCKER_PASSWORD=""

while getopts ":u:p:" opt; do
  case $opt in
    u) DOCKER_USER="$OPTARG"
    ;;
    p) DOCKER_PASSWORD="$OPTARG"
    ;;
  esac
done

command_exists() {
	command -v "$@" > /dev/null 2>&1
}

if [ "$DOCKER_PASSWORD" = "" ] || [ "$DOCKER_USER" = "" ]; then
	echo ""
	echo "[ERRO]: User and password is required for docker service."
	echo "        start.sh -u <username> -p <password>"
	echo ""
	exit
fi

if command_exists curl; then
	echo "Starting..."
	
	if ! command_exists docker; then
		echo "Installing docker..."
		echo "Retrieving docker installer..."
		curl https://get.docker.com | sudo bash
		echo "Updating docker user..."
	fi

	sudo usermod -aG docker ubuntu
	echo "Login on docker... $DOCKER_USER"
	docker login -u=$DOCKER_USER -p=$DOCKER_PASSWORD
	
	echo "Finished"
else
	echo "Command curl not exists, please install curl and start again!"
fi